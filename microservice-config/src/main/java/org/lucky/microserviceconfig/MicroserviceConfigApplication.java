package org.lucky.microserviceconfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigServer
public class MicroserviceConfigApplication {

	@Value("${eureka.client.serviceUrl.defaultZone}")
	private String path;

	private static final Logger logger = LoggerFactory.getLogger(MicroserviceConfigApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceConfigApplication.class, args);
	}

	@PostConstruct
	public void  propertyPath(){
		logger.info("############################## proprty #######################\n"+path);
	}

}
