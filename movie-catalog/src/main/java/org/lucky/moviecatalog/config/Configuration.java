package org.lucky.moviecatalog.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory=new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(5);
        clientHttpRequestFactory.setReadTimeout(5);
        clientHttpRequestFactory.setConnectionRequestTimeout(5);
        return new RestTemplate(clientHttpRequestFactory);
    }
}
