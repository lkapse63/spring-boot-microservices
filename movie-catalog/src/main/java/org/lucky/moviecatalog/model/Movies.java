package org.lucky.moviecatalog.model;

public class Movies {

    private String movieId;
    private String name;

    public Movies(String movieId, String name) {
        this.movieId = movieId;
        this.name = name;
    }

    public Movies() {
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Movies{" +
                "movieId='" + movieId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
