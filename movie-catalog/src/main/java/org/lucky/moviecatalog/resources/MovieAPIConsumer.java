package org.lucky.moviecatalog.resources;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.lucky.moviecatalog.model.MovieCatalogModel;
import org.lucky.moviecatalog.model.Movies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
public class MovieAPIConsumer {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${microserice.url}")
    private String microserviceUrl;

    @HystrixCommand(fallbackMethod = "getFallbackCatalog",
            commandProperties = {
                @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="2000"),
                @HystrixProperty(name="circuitBreaker.requestVolumeThreshold",value="5"),
                @HystrixProperty(name="circuitBreaker.errorThresholdPercentage",value="50"),
                @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds",value="5000")
            }
    )
    public Movies getMovies(String userId) {
        return restTemplate.getForObject(microserviceUrl+"/"+userId, Movies.class);
    }

    public  Movies getFallbackCatalog(String userId){
        return  new Movies("No movie", "No Movie");
    }
}
