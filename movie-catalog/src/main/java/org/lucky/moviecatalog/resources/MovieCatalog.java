package org.lucky.moviecatalog.resources;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.lucky.moviecatalog.model.MovieCatalogModel;
import org.lucky.moviecatalog.model.Movies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/catalog")
@RefreshScope
public class MovieCatalog {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${microserice.url}")
    private String microserviceUrl;

    @Autowired MovieAPIConsumer movieAPIConsumer;

    @Value("${external:dummy}")
    private String springExternal;

    private static final Logger logger = LoggerFactory.getLogger(MovieCatalog.class);

    @GetMapping("/{userId}")

    public List<MovieCatalogModel> getCatalog(@PathVariable("userId") String userId){
        Movies movies = movieAPIConsumer.getMovies(userId);
        logger.info("movies return: "+movies.toString());
        return Collections.singletonList(
                new MovieCatalogModel(movies.getName(),"marvel movie",5)
        );

    }




}
