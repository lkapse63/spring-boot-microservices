package org.lucky.movieinfoservice.services;

import org.lucky.movieinfoservice.model.Movies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/movies")
public class MovieService {

    private static final Logger logger = LoggerFactory.getLogger(MovieService.class);

    @GetMapping("/{movieId}")
    public Movies getMovie(@PathVariable("movieId") String movieId) throws InterruptedException {
        logger.info("request recived.. "+movieId);
//        Thread.currentThread().join(10000);
        return new Movies(movieId,movieId);
    }
}
